## CSE 555 - Assignment 3 - Probabilistic Graphical Models
### Making use of the Bayesian Belief Networks page provided by eBay
#### Running the code
Run the code using the following command, `python2 main.py`.

Requires the following libraries be installed:
1. eBay/bayesian-belief-networks
2. pydot
