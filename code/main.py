#!/usr/bin/env python2
from __future__ import print_function
import os
import bif_parser
import pydot
from bayesian.bbn import *


def dump_gviz_image(gviz_data, filename="out/graph.png"):
    (graph,) = pydot.graph_from_dot_data(gviz_data)
    graph.write_png(filename)
    return filename


def find_clusters_for_variable(nodes, variable="bronc"):
    return [node for node in nodes if variable in node.variable_names]


def print_clique_marginals(clique):
    for situation, probability in clique.iteritems():
        condition_strings = ["{0}: {1}".format(condition[0], condition[1]) for condition in situation]
        print("Conditions: ({0}), Probability: {1}".format(", ".join(condition_strings), probability))
    return


def sum_assignments(potentials, condition):
    return sum([v for k, v in potentials.iteritems() for i in k if i == condition])


def find_marginal_probabilities(nodes, variable):
    marginals = {'yes': 0.0, 'no': 0.0}
    clusters = find_clusters_for_variable(nodes, variable)
    if len(clusters):
        head = clusters[0]
        potential = head.potential_tt
        yes, no = [sum_assignments(potential, (variable, yn)) for yn in ['yes', 'no']]
        total = float(yes + no)
        marginals = {'yes': yes / total, 'no': no / total}
    return marginals


def find_joint_probability(nodes, situtations, evidences):
    situ_prob = 1.0
    evid_prob = 1.0
    joint_prob = 1.0
    for situation in situtations:
        (variable, yn) = situation
        prob = find_marginal_probabilities(nodes, variable)[yn]
        situ_prob *= prob
    for evidence in evidences:
        (variable, yn) = evidence
        prob = find_marginal_probabilities(nodes, variable)[yn]
        evid_prob *= prob
    if len(evidences):
        joint_prob = (situ_prob * evid_prob) / evid_prob
    else:
        joint_prob = situ_prob
    return joint_prob


def main():
    name = "asia"

    module_name = bif_parser.parse(name)
    module = __import__(module_name)
    bbn = module.create_bbn()

    # Output relation as graph
    filename = "out/graph.png"
    sf = bbn.get_graphviz_source()
    if not os.path.isfile(filename):
        print("Rendering relations as graph to %s" % filename)
        dump_gviz_image(sf, filename)
    else:
        print("Relations graph already exists at %s" % filename)

    # Output moralised graph
    filename = "out/moralized.png"
    gu = make_undirected_copy(bbn)
    m1 = make_moralized_copy(gu, bbn)
    s2 = m1.get_graphviz_source()
    if not os.path.isfile(filename):
        print("Rendering relations as moralized graph to %s" % filename)
        dump_gviz_image(s2, filename)
    else:
        print("Moralized graph already exists at %s" % filename)

    # Output triangulation of graph
    filename = "out/triangulated.png"
    cliques, elimination_ordering = triangulate(m1, priority_func)
    s2 = m1.get_graphviz_source()
    if not os.path.isfile(filename):
        print("Rendering relations as triangulated graph to %s" % filename)
        dump_gviz_image(s2, filename)
    else:
        print("Triangulated graph already exists at %s" % filename)

    # Output junction tree
    filename = "out/junction.png"
    jt = bbn.build_join_tree()
    sf = jt.get_graphviz_source()
    if not os.path.isfile(filename):
        print("Rendering relations as junction/join tree to %s" % filename)
        dump_gviz_image(sf, filename)
    else:
        print("Junction/join tree already exists at %s" % filename)

    assignments = jt.assign_clusters(bbn)
    jt.initialize_potentials(assignments, bbn)
    jt.propagate()

    for clique_node in jt.clique_nodes:
        print_clique_marginals(clique_node.potential_tt)
    situations = (('tub', 'yes'), ('lung', 'yes'), ('bronc', 'yes'))
    evidences = (('asia', 'yes'), ('xray', 'yes'))
    joint_probability = find_joint_probability(jt.clique_nodes, situations, evidences)
    print("Joint probability of {0} given {1} is {2}".format(situations, evidences, joint_probability))
    return


if __name__ == "__main__":
    main()
